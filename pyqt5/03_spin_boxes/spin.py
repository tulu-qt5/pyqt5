import PyQt5.QtWidgets as qtw
import PyQt5.QtGui as qtg

class MainWindow(qtw.QWidget):
    def __init__(self):
        super().__init__()

        # add a title
        self.setWindowTitle("Spin Boxes")

        #set vertical Layout
        self.setLayout(qtw.QVBoxLayout())

        # create a label
        my_label = qtw.QLabel("Hello What's your name?")
        # change the font size of label
        my_label.setFont(qtg.QFont('Helvertica', 18))

        self.layout().addWidget(my_label)

        # create a spin box
        # spinbox for integers
        # double spin boxes for floats
        my_spin = qtw.QDoubleSpinBox(self,
                               value=10.5,
                               maximum=100,
                               minimum=0,
                               singleStep=10,
                               prefix='#',
                               suffix=' Order')
        # change font size of spin box
        my_spin.setFont(qtg.QFont('Helvertica', 18))
        
        # add spin box in vertical layout
        self.layout().addWidget(my_spin)


        # create a button
        my_botton = qtw.QPushButton("press",
                                    clicked = lambda: pressed())

        self.layout().addWidget(my_botton)
        # show main screen
        self.show()

        def pressed():
            # change label
            my_label.setText(f"Hello {my_spin.value()}")


app = qtw.QApplication([])
mw = MainWindow()

# run app
app.exec_()