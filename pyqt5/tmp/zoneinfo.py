import os
import subprocess

def get_all_locales():
    locale_file_contents = subprocess.check_output("cat /etc/locale.gen", shell=True).decode('utf-8')
    locale_file_contents = locale_file_contents.split('\n\n')[1]
    array_raw_locales = locale_file_contents.split('\n')
    array_locales = []

    for raw_locale in array_raw_locales:
        if raw_locale != '':
                array_locales.append(raw_locale.replace('# ', ''))

    return array_locales

all_locales = get_all_locales()
for locales in all_locales:
     print(locales)