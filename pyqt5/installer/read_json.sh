echo efi partition

jq '.partition' install_config.json | jq '.efi'

echo root partition

jq '.partition' install_config.json | jq '.root'

echo localle area

jq '.locale' install_config.json | jq '.area'

echo locale region

jq '.locale' install_config.json | jq '.region'

echo locale locale

jq '.locale' install_config.json | jq '.locale'

echo user full_name

jq '.user' install_config.json | jq '.full_name'

echo user user_name

jq '.user' install_config.json | jq '.user_name'

echo user host_name

jq '.user' install_config.json | jq '.host_name'

echo user user_password

jq '.user' install_config.json | jq '.user_password'

echo user root_password

jq '.user' install_config.json | jq '.root_password'
