# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'partition.ui'
#
# Created by: PyQt5 UI code generator 5.15.10
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")

        self.verticalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 20, 771, 481))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")

        self.outerVerticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.outerVerticalLayout.setContentsMargins(0, 0, 0, 0)
        self.outerVerticalLayout.setObjectName("outerVerticalLayout")

        # for partition label
        self.partition_label = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.partition_label.setFont(font)
        self.partition_label.setAlignment(QtCore.Qt.AlignCenter)
        self.partition_label.setObjectName("partition_label")
        self.outerVerticalLayout.addWidget(self.partition_label)

        # setting the inner horizontal layout
        self.innerHorizontalLayout = QtWidgets.QHBoxLayout()
        self.innerHorizontalLayout.setObjectName("innerHorizontalLayout")
        
        # set open gparted button
        self.partition_gparted_button = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.partition_gparted_button.setObjectName("partition_gparted_button")
        self.innerHorizontalLayout.addWidget(self.partition_gparted_button)

        # set refresh button
        self.partition_refresh_button = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.partition_refresh_button.setObjectName("partition_refresh_button")
        self.innerHorizontalLayout.addWidget(self.partition_refresh_button)

        # adding the inner horizontal layout in outer horizontal layout
        self.outerVerticalLayout.addLayout(self.innerHorizontalLayout)

        # text edit box for displaying all disks
        self.textEdit = QtWidgets.QTextEdit(self.verticalLayoutWidget)
        self.textEdit.setObjectName("textEdit")
        self.outerVerticalLayout.addWidget(self.textEdit)

        # combo box for efi partition
        self.choose_efi = QtWidgets.QComboBox(self.verticalLayoutWidget)
        self.choose_efi.setObjectName("choose_efi")
        self.outerVerticalLayout.addWidget(self.choose_efi)

        # combo box for root partition
        self.choose_root = QtWidgets.QComboBox(self.verticalLayoutWidget)
        self.choose_root.setObjectName("choose_root")
        self.outerVerticalLayout.addWidget(self.choose_root)

        # back button for main window
        self.back_button = QtWidgets.QPushButton(self.centralwidget)
        self.back_button.setGeometry(QtCore.QRect(600, 520, 90, 28))
        self.back_button.setObjectName("back_button")
        self.back_button.clicked.connect(self.remove_widgets)

        # next button for main window
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(710, 520, 90, 28))
        self.pushButton_2.setObjectName("pushButton_2")


        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 22))


        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.partition_label.setText(_translate("MainWindow", "Disks in your computer"))
        self.partition_gparted_button.setText(_translate("MainWindow", "Open GParted"))
        self.partition_refresh_button.setText(_translate("MainWindow", "Refresh"))
        self.back_button.setText(_translate("MainWindow", "Back"))
        self.pushButton_2.setText(_translate("MainWindow", "Next"))

    def remove_widgets(self):
        layout = self.outerVerticalLayout
        for i in reversed(range(layout.count())): 
            layout.itemAt(i).widget().deleteLater()


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
