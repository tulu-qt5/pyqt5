#!/bin/bash

echo "==>08.GENERATING LOCALE"

locale=$(jq '.locale' /installer/install_config.json | jq '.locale')

echo "=>COMMAND=echo $curr_locale >> /etc/locale.gen"
echo $curr_locale >> /etc/locale.gen

echo "=>COMMAND=locale-gen"
locale-gen

unset curr_locale
