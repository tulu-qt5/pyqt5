#!/bin/bash

echo "==>MOUNTING EFI PARTITION"
# creating efi folder
echo "=>COMMAND=mkdir /mnt/boot/efi"
mkdir /mnt/boot/efi

# getting the efi partition
boot=$(jq '.partition' install_config.json | jq '.efi')

#mounting efi partition
echo "=>COMMAND=mount $boot /mnt/boot/efi"
mount $boot /mnt/boot/efi
