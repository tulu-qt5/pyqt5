#!/bin/bash
echo "==>10.ENTERING ROOT PASSWORD CREATING USERNAME AND PASSWORD, REMOVING LIVE USERNAME"

full_name=$(jq '.user' /installer/install_config.json | jq '.full_name')
user_name=$(jq '.user' /installer/install_config.json | jq '.user_name')
user_password=$(jq '.user' /installer/install_config.json | jq '.user_password')
root_password=$(jq '.user' /installer/install_config.json | jq '.root_password')

printf "$root_password\n$root_password" | passwd
useradd -mc $full_name $user_name
usermod -aG wheel,audio,video,optical,storage $user_name
cp -r /home/live/ /home/$user_name/
printf "$user_password\n$user_password" | passwd $user_name
sed -i "s/# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/g" /etc/sudoers

userdel -r live
