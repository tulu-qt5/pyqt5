#!/bin/bash
echo "RUNNING COMMANDS IN CHROOT ENVIRON"

source /installer/scripts/07.timezone.sh
source /installer/scripts/08.locale.sh
source /installer/scripts/09.hosts.sh
source /installer/scripts/10.rootusr.sh
source /installer/scripts/11.initramfs.sh
source /installer/scripts/12.grub.sh
