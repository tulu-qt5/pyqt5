#!/bin/bash
echo "==>12.CREATING GRUB CONFIG"

grub-install --target=x86_64-efi /boot/efi
grub-mkconfig -o /boot/grub/grub.cfg
