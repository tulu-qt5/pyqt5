#!/bin/bash

echo "==>FORMATTING THE DISK"

boot=$base$(jq '.partition' install_config.json | jq '.efi')
root=$base$(jq '.partition' install_config.json | jq '.root')

# formatting the drives
mkfs.fat -F32 $boot
mkfs.ext4 $root
