#!/bin/bash

echo "==>06 RUNNING SCRIPTS IN ROOT ENVIRONMENT IN /MNT"
# before changing to chroot in /mnt we have to copy required scripts in /mnt directory to run them
# create a folder to store the scripts
echo "=>COMMAND=mkdir /mnt/installer"
mkdir /mnt/installer
#copy required scripts in the scripts folder
#control all scripts using runch.sh
echo "=>COMMAND=cp -r ./* /mnt/installer"
cp -r ./* /mnt/installer
# change to chroot and run the runch.sh script
echo "=>COMMAND=arch-chroot /mnt /installer/scripts/runch.sh"
arch-chroot /mnt /installer/scripts/runch.sh
