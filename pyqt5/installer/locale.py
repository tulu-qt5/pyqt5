# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'locale.ui'
#
# Created by: PyQt5 UI code generator 5.15.10
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(20, 20, 751, 471))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")

        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")

        self.lcoale_horizontal_layout_1 = QtWidgets.QHBoxLayout()
        self.lcoale_horizontal_layout_1.setObjectName("lcoale_horizontal_layout_1")

        self.locale_area_comboBox = QtWidgets.QComboBox(self.verticalLayoutWidget)
        self.locale_area_comboBox.setObjectName("locale_area_comboBox")
        self.lcoale_horizontal_layout_1.addWidget(self.locale_area_comboBox)

        self.locale_region_comboBox = QtWidgets.QComboBox(self.verticalLayoutWidget)
        self.locale_region_comboBox.setObjectName("locale_region_comboBox")
        self.lcoale_horizontal_layout_1.addWidget(self.locale_region_comboBox)

        self.verticalLayout.addLayout(self.lcoale_horizontal_layout_1)

        self.locale_locale_combo_box = QtWidgets.QComboBox(self.verticalLayoutWidget)
        self.locale_locale_combo_box.setObjectName("locale_locale_combo_box")
        self.verticalLayout.addWidget(self.locale_locale_combo_box)
        
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
