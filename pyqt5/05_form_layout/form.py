import PyQt5.QtWidgets as qtw
import PyQt5.QtGui as qtg

class MainWindow(qtw.QWidget):
    def __init__(self):
        super().__init__()

        # add a title
        self.setWindowTitle("This is simple form")

        # set form layout
        form_layout = qtw.QFormLayout()
        self.setLayout(form_layout)

        # add widgets
        label_1 = qtw.QLabel("this is label 1")
        label_1.setFont(qtg.QFont("Helvertica", 24))

        f_name = qtw.QLineEdit(self)
        l_name = qtw.QLineEdit(self)

        # add widets in row
        form_layout.addRow(label_1)
        form_layout.addRow("First Name", f_name)
        form_layout.addRow("Second Name", l_name)
        form_layout.addRow(qtw.QPushButton("Press", clicked=lambda: pressed()))


        #show the app
        self.show()

        def pressed():
            label_1.setText(f"you clicked the button, {f_name.text()} {l_name.text()}")
            f_name.setText("")
            l_name.setText("")


app = qtw.QApplication([])
mw = MainWindow()

# run the app
app.exec_()