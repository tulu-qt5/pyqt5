import PyQt5.QtWidgets as qtw
import PyQt5.QtGui as qtg

class MainWindow(qtw.QWidget):
    def __init__(self):
        super().__init__()

        # add a title
        self.setWindowTitle("Spin Boxes")

        #set vertical Layout
        self.setLayout(qtw.QVBoxLayout())

        # create a label
        my_label = qtw.QLabel("Hello What's your name?")
        # change the font size of label
        my_label.setFont(qtg.QFont('Helvertica', 18))

        self.layout().addWidget(my_label)

        # create a text box
        my_text = qtw.QTextEdit(
            self,
            #plainText="this does not disapper",
            #acceptRichText=True, # rich text means color text, italic, bold text etc
            #html="<h1>This is h1 header<h1>",
            lineWrapMode=qtw.QTextEdit.FixedColumnWidth,
            lineWrapColumnOrWidth=50,
            placeholderText="Hello World",
            readOnly=False   # if true can't edit the text
        )
        
        # add spin box in vertical layout
        self.layout().addWidget(my_text)


        # create a button
        my_botton = qtw.QPushButton("press",
                                    clicked = lambda: pressed())

        self.layout().addWidget(my_botton)
        # show main screen
        self.show()

        def pressed():
            # change label
            my_label.setText(f"you typed {my_text.toPlainText()}")
            my_text.setPlainText("you pressed")


app = qtw.QApplication([])
mw = MainWindow()

# run app
app.exec_()