import QtQuick 2.0

Rectangle{
    id: root
    color: "lightgrey"
    width: 500; height: 500

    Item {
        property int _minside: Math.min(root.width, root.height)

        x: 10 + (root.width - _minside)/2
        y: 10 + (root.height - _minside)/2
        height: _minside - 20
        width: _minside - 20

        scale: Math.min(width/background.sourceSize.width, height/background.sourceSize.height)

        transformOrigin: Item.TopLeft // if not mentioned scaling will be done through mid point

        Image{
            id: background
            source: "background.png"
        }

        Image{
            id: smallarm
            source: "smallArm.png"
            x: background.width/2 - width/2
            y: background.height/2 - 914

            transform: Rotation{
                origin.x: smallarm.width /2
                origin.y: 914

                RotationAnimation on angle{
                    from: 0
                    to: 360*2
                    duration: 60000
                    loops: Animation.Infinite
                }
            }
        }

        Image {
            id: largeArm
            source: "largeArm.png"
            x: background.width/2 - width/2
            y: background.height/2 - 1255

            transform: Rotation{
                origin.x: largeArm.width/2
                origin.y: 1255
                angle: 90

                RotationAnimation on angle{
                    from: 0
                    to: 360*24
                    duration: 60000
                    loops: Animation.Infinite
                }
            }
        }
    }
}
