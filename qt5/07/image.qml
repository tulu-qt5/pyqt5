import QtQuick 2.0

Image{
    id: image
    width: 1000; height: 1000
    source: "https://images.pexels.com/photos/2189696/pexels-photo-2189696.jpeg?cs=srgb&dl=pexels-emre-can-acer-2189696.jpg&fm=jpg"
    fillMode: Image.PreserveAspectFit

    Rectangle{
        color: "red"
        x: 0; y: 950
        height: 50
        width: 1000* image.progress // progress is b/w 0 to 1
        visible: image.progress !=1
    }

    onStatusChanged: console.log(sourceSize)
}
