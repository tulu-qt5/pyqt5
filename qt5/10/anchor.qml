import QtQuick 2.0

Rectangle{
    id: root
    width: 400; height: 400
    color: "lightblue"

    Rectangle{
        id: greenrect
        y: 100 // y value overwritten by anchors.top
        width: 200
        height: 200
//        anchors.right: root.right
        anchors{
            right: root.right
            top: root.top
        }

        color: 'green'
    }
}

// anchor values change x, y, width, height values
