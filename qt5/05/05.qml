import QtQuick 2.0

Rectangle{
    id: root
    height: 400; width: 400
    color: 'gray'

    Rectangle{
        id: bluerect
        x: 50; y: 50
        width: root.width -100
        height: (root.height - 100)/2
        color: 'lightblue'

        Rectangle{
            x:50; y: 50
            width: 50; height: 50
            color: 'white'
        }
    }

    Rectangle{
        id: greenrect
        x: bluerect.x; y: bluerect.y + bluerect.height
        width: root.width-100
        height: (root.height-100)/2
        color: 'green'
        clip: true // to tell that child will not cross parent size limit

        Rectangle{
            x: 150; y: 50
            width: 100; height: 50
            color: 'blue'
        }
    }
}
