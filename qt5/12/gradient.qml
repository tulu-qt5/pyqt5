import QtQuick 2.0

Rectangle{
    width: 400; height: 400

    rotation: 45 // to rotate the box
    scale: 1.5 // to remove white traingles
    gradient: Gradient{
        GradientStop{
            position: 0.0; color: 'green'
        }
        GradientStop{
            position: 0.5; color: 'yellow'
        }
        GradientStop{
            position: 1.0; color: 'blue'
        }
    }
}
