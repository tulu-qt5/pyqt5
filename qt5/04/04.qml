import QtQuick 2.0

Item{
    height: 200; width: 400
    TextInput {
        id: mytext
        text: "hello world"
        x: 50; y: 50
        font.family: "Helvertica"; font.pixelSize: 50
    }

    Rectangle {
        x: 50; y: 100; height: 5
        width: mytext.width
        color: 'green'
    }
}
